from django.db import models

# Create your models here.
class Jadwal(models.Model):
    judul = models.CharField(max_length=50)
    tanggal = models.DateField()
    waktu = models.TimeField()
    hari = models.CharField(max_length=20)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
