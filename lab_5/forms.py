from django import forms

days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
class JadwalForm(forms.Form):
    title = forms.CharField(label="Activity", widget=forms.TextInput, max_length=50)
    day = forms.ChoiceField(choices=[(x, x) for x in days], widget=forms.Select)
    date = forms.DateField(label="Date", widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label="Time", widget=forms.TimeInput(attrs={'type' : 'time'}))
    place = forms.CharField(label="Place", widget=forms.TextInput, max_length=50)
    category = forms.CharField(label="Category", widget=forms.TextInput, max_length=50)

