from .forms import JadwalForm
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Jadwal

def index(request):
    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():
            judul = request.POST['title']
            hari = request.POST['day']
            tanggal = request.POST['date']
            waktu = request.POST['time']
            tempat = request.POST['place']
            kategori = request.POST['category']
            isian = Jadwal(judul=judul, hari=hari, tanggal=tanggal, waktu=waktu, tempat=tempat, kategori=kategori)
            isian.save()
            return HttpResponseRedirect('agenda')

    else:
        form = JadwalForm()

    return render(request, 'lab_5/index.html', {'jadwal': form})

def agenda(request):
    if request.method == 'POST':
        Jadwal.objects.all().delete()
        return HttpResponseRedirect('agenda')
    aktivitas = Jadwal.objects.all()
    return render(request,'lab_5/agenda.html', {'aktivitas': aktivitas})
